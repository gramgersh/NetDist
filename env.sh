#
# $Id: env.sh 2062 2009-05-26 23:30:33Z dchang $
# $Source$
#
# Set up your environment to run this version of NetDist.
#
# This should be a symlink in each customer's directory.
#
NDHOME=`pwd`
export NDHOME
#
# Set up the path
__ND_TMP=`(cd $NDHOME/../NetDist && pwd)`
PATH=${__ND_TMP}/bin:$PATH
PATH=`$__ND_TMP/bin/fixpath PATH`
export PATH
unset __ND_TMP

[ -f ./env.$USER ] && . ./env.$USER
