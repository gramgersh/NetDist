#!/bin/sh
#
# $Id: ndmkdir 1289 2008-03-01 22:22:41Z erickson $
#
# create directories and put in Makefiles
#
[ -f $NDHOME/etc/NetDist.conf ] && . $NDHOME/etc/NetDist.conf
#
# parse the args
usage () {
  xval=1
  shift
  [ $# -gt 0 ] && echo "$@"
  echo "usage:  $NDMYNAME [-d domain] [-i instset] dir1 [dir2 ...]
    -d:	the DOMAIN to use
    -i:	the INSTSET to use
    -D: when creating a 'directory' Makefile, set the SUBDIRS to be
        all of the child directories."
    exit $xval
}

USE_DOMAIN=""
USE_INSTSET=""
SUBDIRS_CHILDREN=""

while getopts Dd:i: c ; do
  case "$c" in
    d ) USE_DOMAIN="$OPTARG" ;;
    i ) USE_INSTSET="$OPTARG" ;;
    D ) SUBDIRS_CHILDREN="1" ;;
    \? ) usage 2 unknown argument ;;
  esac
done
shift `expr $OPTIND - 1`

trap CleanUp 1 2 3 13 15

#
# Get a lock on the file so no one else uses it
GetLock $NDMYNAME

for path in $@ ; do
  [ -d "$path" ] || mkdir -p "$path" || continue
  #
  # If there's not a Makefile there, put one there
  if [ ! -f "$path/Makefile" ] ; then
    cp -v $NDHOME/lib/Makefile.dir "$path/Makefile" || continue
  fi
  if [ -n "$USE_DOMAIN" ]; then
    sed -i "s/^DOMAIN=.*/DOMAIN=$USE_DOMAIN/" "$path/Makefile"
  fi
  if [ -n "$USE_INSTSET" ]; then
    sed -i "s/^INSTSET=.*/INSTSET=$USE_INSTSET/" "$path/Makefile"
  fi
  #
  # Now check all of the intermediate paths for Makefiles.
  while [ "$path" != "." -a "$path" != "/" ]; do
    child=`basename "$path"`
    path=`dirname "$path"`
    if [ ! -f "$path/Makefile" ]; then
      cp -v $NDHOME/lib/Makefile.subsystem "$path/Makefile" || continue
    fi
    if [ -n "$SUBDIRS_CHILDREN" ]; then
      for i in "$path"/* ; do
	[ -d "$i" ] || continue
	x=`basename "$i"`
	grep "SUBDIRS += $x" "$path/Makefile" >/dev/null && continue
	echo "SUBDIRS += $x" >> "$path/Makefile"
      done
    else
      grep "SUBDIRS += $child" "$path/Makefile" >/dev/null && continue
      echo "SUBDIRS += $child" >> "$path/Makefile"
    fi
  done
done
CleanUp $NDMYNAME
exit $err
