#!/usr/bin/perl
#
# $Id: ndbatchadd 28 2005-07-26 18:12:41Z erickson $
# $Source: /home/erickson/java.ici/public_html/NetDist/bin/RCS/ndbatchadd,v $
#
=pod

=head1 NAME

ndbatchadd - add files to the NetDist batch queue

=head1 SYNOPSIS

ndbatchadd [ C<-V> ] I<files>

=head1 DESCRIPTION

Adds files to the batch queue.  This is usually run from the Makefile
in an Object directory, and does not need to be run by hand.

=head1 OPTIONS

=over 4

=item C<-V> 

Print status messages about the progress of the script.

=back

=head1 FILES

=over 4

=item $NDBATCHDIR/I<n>

The batch files, where I<n> is an integer

=item $NDLOCKDIR/batch.I<n>

The lockfile used while creating the batch file.

=back

=cut

BEGIN {
    require  "$ENV{'NDHOME'}/etc/NetConf.pl";
}
#
# get the args
use Getopt::Std;
&getopts("V");
Verb::ose($opt_V);
#
# check directories
&CheckDirs($NDBATCHDIR);
#
# Set our counter.  To do this, we need to get the highest number
# already in the batch queue and add 1.  If there's nothing there, or
# if we get an error when we do the add, then set our counter to 1.
@BatchFiles = <$NDBATCHDIR/[0-9]*>;
# Sort the list, and find the highest number
@BatchFiles = sort {$a <=> $b;} @BatchFiles;
$BatchNum = ".";
$BatchNum = shift(@BatchFiles)
    while ("$BatchNum" ne "" && $BatchNum !~ /^[0-9][0-9]*/);
$BatchNum = 0 if ( "$BatchNum" eq "" );
#
# Process each file
foreach $src ( @ARGV ) {
        # ignore empty files
	next if ( ! -s $src );
	# Find a new batch file.  keep looking while expr returns a
	# positive value.  Make sure we can get a lock file.
	#
	# $BatchNum is going to be the number of the last batchfile in the
	# batch directory.
	do {
	    $BatchNum++;
	    #
	    # Look for the next available batch file in $NDBATCHDIR
	    $BatchNum++ while ( $BatchNum > 0 && -f "$NDBATCHDIR/$BatchNum" ) ;
	    #
	    # If we can't get the lock file, the some other process is
	    # trying to get the same file.  Increment BatchNum and start
	    # again.
	} while ( $BatchNum > 0
		&& &GetTmpLock("batch.$BatchNum") != 0);
	&errorexit("no more possible batch files") if ($BatchNum <= 0) ;
	#
	# Now we can copy the batch file at our liesure
	Verb::EchoC("$NDMYNAME:  installing $src as $BatchNum . . . ",1);
	system("cp $src $NDBATCHDIR/$BatchNum");
	Verb::Echo("done",1);
	#
	# Remove our lock file
	&RmTmpLock();
}
#
# Clean up
&CleanUp("$NDMYNAME");
