#!/usr/bin/perl
#
# $Id: ndclrlocks 28 2005-07-26 18:12:41Z erickson $
# $Source: /home/erickson/java.ici/public_html/NetDist/bin/RCS/ndclrlocks,v $
#
# ndclrlocks [-V] [-f]
#    remove the lockfiles.  Force remove if -f switch given
#
=pod

=head1 NAME

ndclrlocks - clear NetDist lock files

=head1 SYNOPSIS

ndclrlocks [C<-V>] [C<-f>]

=head1 DESCRIPTION

B<ndclrlocks> will show the key to every lock in the $NDLOCKDIR, then
ask whether or not to remove the lock.

This needs to be run only if one of the NetDist programs was killed in
a mean way.

=head1 OPTIONS

=over 4

=item C<-V>

Print status messages about the progress of the script.

=item C<-f>

Don't ask for verification.  Just remove it.

=back

=head1 FILES

=over 4

=item $NDLOCKDIR/*

Lock files.

=back

=cut

BEGIN {
    require  "$ENV{'NDHOME'}/etc/NetConf.pl";
}
#
# Check the directory
&CheckDirs("$NDLOCKDIR");
chdir("$NDLOCKDIR") || &errorexit("$NDMYNAME:  cannot cd to $NDLOCKDIR: $!");
#
# get the args
use Getopt::Std;
&getopts("Vf");
Verb::ose($opt_V);
#
# If we're forcing it, then just do it
if ( $opt_f ) {
    Verb::EchoC("$NDMYNAME:  removing all lock files . . . ",1);
    unlink <*>;
    Verb::Echo("done.",1);
    exit(0);
}
#
# Otherwise, ask before unlinking each file
#
foreach ( <*> ) {
    next if ( $_ eq "." || $_ eq ".." );
    $msg = "";
    if ( open(INFILE,$_) ) {
	chop($msg = <INFILE>) ;
	close(INFILE);
    }
    print "remove $_ ($msg)? [n] ";
    chop($ans = <STDIN>);
    if ( $ans =~ /^[Yy]/ ) {
	unlink $_;
    }
    else {
	print "$_ not removed.\n";
    }
}
exit 0
