#
# $Id: Hosts.pm 28 2005-07-26 18:12:41Z erickson $
# $Source: /opt/NetDist/twcny/lib/perl/Site/RCS/Hosts.pm,v $
#
package Site::Hosts;
do "$ENV{'NDHOME'}/etc/NetConf.pl";
do "$NDHOME/etc/site.pl";

=head1 NAME

Site::Hosts - access to site host information

=head1 SYNOPSIS

	use Site::Host;
	$sh = new Site::Host ;
	$sh->name2ip('mail');	        # return IP address of mail
	$sh->ip2name('127.0.0.1');	# return name
	$sh->shortname('host-atm') ;	# return short name
	$sh->aliases('host');		# return array of aliases for host
	$sh->suffixes();		# return array of known suffixes
	$sh->all_addrs();		# return array of all addresses
	$sh->all_hosts();		# return array of all hosts


=head1 DESCRIPTION

The C<Site::Host> method provides access to the host information for
the NetDist site, which may be different than the information for

    the running site.

=cut

#
require 5.000;
use Carp;
#
local(@HostSuffixes);
#
# We can't use gethostby*() routines, because we may be generating files
# for a domain that's not ours.  So, keep some information handy
local(%IpMap);	# IP addresses key'd by hostname (from A records)
local(%Cname);	# Cnames key'd by alias (from CNAME records)
local(%NameMap);# Names key'd by IP address
local(%Aliases);# Arrays of aliases key'd by name
sub new {
    my $class = shift;
    my $self = {};
    #
    # Keep track of the name/IP address.
    $dbfile = "$NDHOME/etc/named/db.$dom";
    croak "cannot open $dbfile for read:  $!\n"
	if ( !open(INFILE,"$dbfile") ) ;
    while ( $_ = <INFILE> ) {
	# remove comments
	s/;.*//;
	next if ( /^$/ ); 			# Skip blank lines
	# If it's a CNAME, make a note of it
	if ( /\s+in\s+cname\s+/i ) {
	    ($name,$cname) = (split)[0,3];
	    $Cname{$name} = $cname;
	    push(@{$Aliases{$cname}},$name);
	    next;
	}
	next if ( ! /\s+in\s+a\s+/i );	# skip it if it's not an A record
	# We have an A record.  Grab the name and IP address
	$name = lc((split)[0]);
	s/.*\s+in\s+a\s+//i;	# remove text up to the address
	$_ = (split)[0];		# grab the address
	# If it's the loopback or fake network, ignore it
	next if ( /^127.0.0/ || /^0.0.0/ );
	# Make note of the IP address of each host
	$IpMap{$name} = $_ if ( $IpMap{$name} eq "" );
	# If we have another A record for the same IP, then make it an
	# alias instead.
	if ( $NameMap{$_} eq "" ) {
	    $NameMap{$_} = $name;
	}
	else {
	    push(@{$Aliases{$NameMap{$_}}},$name);
	}
	# Create a network identifier out of the IP address (assume Class C)
	@ip = split(/\./,$_);
    }
    close INFILE;
    #
    # For all of the CNAME records, make sure it's in the IpMap list
    foreach ( keys(%Cname) ) {
	if ( ! $IpMap{$_} ) {
	    $IpMap{$_} = $IpMap{$Cname{$_}};
	}
    }
    #
    @HostSuffixes = ( "-0", "-1" );
    bless $self,$class;
}
#
# sub name2ip($name) -- return the IP address for $name
# requested.
sub name2ip {
    my($self,$name) = @_;
    return $IpMap{$name};
}
#
# sub aliases($host) -- return array of aliases for $host
sub aliases {
    my($self,$host) = @_;
    return @{$Aliases{$host}};
}
# sub suffixes() -- return array of known suffixes
sub suffixes { return @HostSuffixes; }
#
# sub ip2name($ip) -- return the name for $ip address
sub ip2name {
    my($self,$ip) = @_;
    return $NameMap{$ip};
}
#
# shortname($name) -- $name without the suffix
sub shortname {
    my($self) = shift(@_);
    local($_) = shift(@_);
    local($suffix);
    
    foreach $suffix (@HostSuffixes) {
	if  ( m/$suffix$/ ) {
	    s/$suffix$//;
	    return $_;
	}
    }
    return $_;
}
#
# all_addrs() -- return array of all addresses
sub all_addrs { return keys(%NameMap); }
# all_hosts() -- return array of all addresses
sub all_hosts { return keys(%IpMap); }

1;    
