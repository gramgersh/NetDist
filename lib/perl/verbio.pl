;#
;# $Id: verbio.pl 28 2005-07-26 18:12:41Z erickson $
;# $Source: /home/erickson/public_html/java.ici/public_html/NetDist/NetDist/lib/perl/RCS/verbio.pl,v $
;#
;# Misc. "Verbose" Routines that can be used by many scripts.
;#
package Verb;

=head1 NAME

verbio.pl -- package implementing 'verbose' message reporting

=head1 SYNOPSIS

    require verbio.pl;

    Verb::ose($mode);
    Verb::EchoC($str,$level);
    Verb::Echo($str,$level);
    Verb::Twirl($count,$level);
    Verb::CountInit($val);
    Verb::Countdown($level);

=head1 DESCRIPTION

The verbio.pl package contains functions for printing verbose or debug
messages depending on a settable level.  This allows packages to print
few messages when the debug level is 1, and to print many detailed messages
with a higer debug level.

The Verb::Twirl routine prints a series of characters in place that
when displayed quickly, looks like a twirly thing.  For example, the
following bit of code will read a list of files from STDIN and print a
twirly thing for each 10 files processed so that you know that
something is running.  The messages get printed only if the Verb::ose()
level is set to 2 or higer.

    Verb::EchoC("Reading files  ", 2);
    while ( $fname = <STDIN> ) {
	Verb::Twirl(10,2)
	chomp;
	$st = stat($fname) || push(@NoFile,$fname);
    }
    Verb::Echo("finished.",2)


The Verb::Countdown routine prints a number in place and can be used
inform the user of a countdown.  For example, the following bit of code
will process all of the files on the command line and print a
countdown number for each file processed.  The messages get printed only
if the Verb::ose() level is set to 2 or higer.

    Verb::EchoC("Files to process:  ",2);
    Verb::CountInit($#ARGV + 1);
    foreach $fname ( @ARGV ) {
	Verb::Countdown(2);
	process_file($fname);
    }
    Verb::Echo("finished.",2);


=head1 FUNCTIONS

=over 4

=item Verb::ose($level)

Set the verbose level.  When the other Verb::* routines are called,
messages will be printed only if the level passed in the routine is
less than or equal to the level set with this routine.

=back

    Returns the previously set level.
    With no args, just return the current level.

=cut

$VERBOSE = 0;
$COUNTDOWN = 0;
$COUNTWID = 0;
sub ose {
    local($mode);
    return $VERBOSE if ( @_ < 0 ) ;		# just query
    $mode = $VERBOSE;
    $VERBOSE = shift(@_);
    return $mode;
}

=pod

=over 4

=item Verb::EchoC($str,$level)

echo string with no newline if verbose level >= level

=back

=cut

sub Echo {
    local($str) = shift(@_);
    local($lev) = shift(@_);
    return if ( $lev > $VERBOSE );
    print STDERR "$str\n";
}

=pod

=over 4

=item Verb::Echo($str,$level)

echo string with newline if verbose level >= level

=back

=cut

sub EchoC {
    local($str) = shift(@_);
    local($lev) = shift(@_);
    return if ( $lev > $VERBOSE );
    print STDERR "$str";
}

=pod

=over 4

=item Verb::Twirl($count,$level)

print a twirly thing every count calls if verbose level >= level

=back

=cut

$twirl_count = 0;			# count number
$twirl_char = 0;			# which character to print
sub Twirl {
    local($count) = shift(@_);
    local($lev) = shift(@_);
    return if ( $lev > $VERBOSE );
    #
    # see if we've completed a cycle
    $twirl_count = 0 if ($twirl_count >= $count);
    #
    # see if it's time to print one.  We will print one when
    # twirl_count is zero.
    if ( $twirl_count == 0 ) {
	if    ( $twirl_char == 0 ) { print STDERR "|" ; $twirl_char++ ;}
	elsif ( $twirl_char == 1 ) { print STDERR "/" ; $twirl_char++ ;}
	elsif ( $twirl_char == 2 ) { print STDERR "-" ; $twirl_char++ ;}
	elsif ( $twirl_char == 3 ) { print STDERR "\\" ; $twirl_char = 0 ;}
    }
    $twirl_count++;
}

=pod

=over 4

=item Verb::Countdown($level)

print out the number of counts left if verbose level >= level

=back

=cut

sub Countdown {
    local($lev) = shift(@_);
    local($str,$fmt);
    return if ( $lev > $VERBOSE || $COUNTDOWN < 0);
    $fmt = "%0${COUNTWID}d";
    $str = sprintf($fmt,$COUNTDOWN);
    print STDERR $str . "\b" x length($str) ;
    $COUNTDOWN--;
}

=pod

=over 4

=item Verb::CountInit($val)

Initialize the Countdown value for the Verb::Countdown routine.

=back

=cut

sub CountInit {
    $COUNTDOWN = shift(@_);
    $COUNTWID = length(sprintf("%d",$COUNTDOWN));
}
1;
