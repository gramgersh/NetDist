#
#

=pod

=head1 NAME

NetConf.pl -- configuration file for NetDist system

=head1 SYNOPSIS

=head2 perl

(This is available as a template file in $NDHOME/bin/header.pl)

    BEGIN {
	require "$ENV{'NDHOME'}/etc/NetConf.pl";
    }

=head2 bourne shell

(This is available as a template file in $NDHOME/bin/header)


    case "$NDCONF" in "" ) NDCONF=$NDHOME/etc/NetDist.conf ;; esac
    if test ! -f $NDCONF ; then
	    echo "Cannot find $NDCONF config file."
	    echo "Make sure NDCONF or NDHOME envar is set"
	    exit 1
    fi
    . $NDCONF
    #
    # Catch Signals.
    #
    # Stop on hangup, interrupt, quit, pipe-error, software termination.
    # The CleanUp function requires that a list of open locks be kept in
    # NDLOCKS and a list of temporary files be kept in NDTMPFILES.
    trap CleanUp 1 2 3 13 15
    #

=head1 DESCRIPTION

The NetConf.pl file contains all of the NetDist system configuration
paramaters, as well as many useful functions that can be used in any
NetDist script, including object Generate scripts.

The NetConf.pl file is set up during the initial install to set site
options, for example, the program to use for I<rdist>, the method for
expanding netgroups, etc.

=head1 ENVIRONMENT VARIABLES

All environment variables in the %ENV array are imported into the
global namespace.  Then, the following variables are set and exported
to %ENV if no value was imported from the environment.

=cut

#################
#
# IMPORTED ENVIRONMENT
#
#################
BEGIN {
    my($tmp,$key) = ();

    foreach $key (keys(%ENV)) {
	 $tmp .= "\$$key = \$ENV{'$key'};" if $key =~ /^[A-Za-z]\w*$/;
    }
    eval $tmp;
}
#################
#
# START OF EXPORTED ENVIRONMENT
#
#################
#
# START OF DIRECTORIES
#    

=pod

=head2 DIRECTORIES

=over 4

=item $NDHOME

Points to the top of the NetDist directory.  Default is "/opt/NetDist".

=back

=cut

$ENV{'NDHOME'}="/opt/NetDist" if (!defined($ENV{'NDHOME'}));
#
# We need to set NDLIBDIR early, because that's where we put our
# perl scripts.

=pod

=over 4

=item $NDLIBDIR

Points to the NetDist lib directory.  This directory is then added to
the @INC array to allow inclusion of NetDist perl modules.

Default is $NDHOME/lib.

=back

=cut

$ENV{'NDLIBDIR'}="$ENV{'NDHOME'}/lib" if (!defined($ENV{'NDLIBDIR'}));
#
# look in our lib dir for "do" and "require" scripts
push (@INC,"$ENV{'NDLIBDIR'}/perl");
require 'verbio.pl';

=pod

=over 4

=item $PATH

$NDHOME/bin is prepended to the $PATH variable.

=back

=cut

$ENV{'PATH'}="$NDHOME/bin:$PATH";

=pod

=over 4

=item $NDSCRIPTDIR

Points to the directory containing shared scripts.  sed, awk, or other
scripts used by more than one Object Genenerate script can be kept
here.

Default is $NDHOME/scripts.

=back

=cut

$ENV{'NDSCRIPTDIR'}="$ENV{'NDHOME'}/scripts" if (!defined($ENV{'NDSCRIPTDIR'}));

=pod

=over 4

=item $NDSPOOLDIR

Points the the spool directory.

Defult is $NDHOME/spool.

=back

=cut

$NDSPOOLDIR="$NDHOME/spool" if (!defined($NDSPOOLDIR));
$ENV{'NDSPOOLDIR'} = $NDSPOOLDIR ;

=pod

=over 4

=item $NDTMP

Points to a temporary directory.

Default is $NDSPOOLDIR/tmp.

=back

=cut

$NDTMPDIR="$NDSPOOLDIR/tmp" if (!defined($NDTMPDIR));
$ENV{'NDTMPDIR'} = $NDTMPDIR ;

=pod

=over 4

=item $NDBATCHDIR

Directory holding the queued up batch files.

Default $NDSPOOLDIR/batch.

=back

=cut

$NDBATCHDIR="$NDSPOOLDIR/batch" if (!defined($NDBATCHDIR));
$ENV{'NDBATCHDIR'} = $NDBATCHDIR ;

=pod

=over 4

=item $NDLOCKDIR

Points to the lock directory (for atomic actions).

Default is $NDSPOOLDIR/lock.

=back

=cut

$NDLOCKDIR="$NDSPOOLDIR/lock" if (!defined($NDLOCKDIR));
$ENV{'NDLOCKDIR'} = $NDLOCKDIR ;

=pod

=over 4

=item $NDICONFDIR

Points to the NetDist Imake config directory.

Default is $NDHOME/imake-config.

=back

=cut

$NDICONFDIR="$NDHOME/imake-config" if (!defined($NDICONFDIR));
$ENV{'NDICONFDIR'} = $NDICONFDIR ;

=pod

=over 4

=item $NDHOSTFILEDIR

Points to NetDist hostfiles directory, which contains the host
instruction file.

Default is $NDHOME/hostfiles.

=back

=cut

$NDHOSTFILEDIR="$NDHOME/hostfiles" if (!defined($NDHOSTFILEDIR));
$ENV{'NDHOSTFILEDIR'} = $NDHOSTFILEDIR ;

=pod

=over 4

=item $NDHOME/etc

Points to a directory containing host maps, directory maps, and other
    map files.

Default is $NDHOSTFILEDIR.

=back

=cut

$NDMAPDIR="$NDHOSTFILEDIR" if (!defined($NDMAPDIR));
$ENV{'NDMAPDIR'} = $NDMAPDIR ;


# END OF DIRECTORIES
#
# FILES

=pod

=head2 FILES

=over 4

=item $DOMAINFILE

Contains the name of a domain file in the current Object directory.
This variable should be used by any script that deals with a domain
file.

Default is .domain.

=back

=cut

$DOMAINFILE=".domain" if (!defined($DOMAINFILE));
$ENV{'DOMAINFILE'} = $DOMAINFILE ;

=pod

=over 4

=item $INSTSETFILE 

Contains the name of a install set file in the current Object directory.
This variable should be used by any script that deals with an
install set file.

Default is .instset.

=back

=cut

$INSTSETFILE=".instset" if (!defined($INSTSETFILE));
$ENV{'INSTSETFILE'} = $INSTSETFILE ;

=pod

=over 4

=item $NDBATCHFILE

Contains the default name of the batch file for the current
Object directory.

Default is .batch.

=back

=cut

$NDBATCHFILE=".batch" if (!defined($NDBATCHFILE));
$ENV{'NDBATCHFILE'} = $NDBATCHFILE ;

=pod

=over 4

=item $NDHOSTINST 

Contains the file that contains the host instructions.

Default is $NDHOSTFILEDIR/hostinst.

=back

=cut

$NDHOSTINST="$NDHOSTFILEDIR/hostinst" if (!defined($NDHOSTINST));
$ENV{'NDHOSTINST'} = $NDHOSTINST ;

=pod

=over 4

=item $NDDISTFILE 

Contains  the name of the Distfile to generate when running nddist.

Default is $NDTMPDIR/Distfile.$$.  The $$ is expanded to be the
process id.

=back

=cut

$NDDISTFILE="$NDTMPDIR/Distfile.$$" if (!defined($NDDISTFILE));
$ENV{'NDDISTFILE'} = $NDDISTFILE ;
#
# END OF FILES
#
# START OF PROGRAMS
#

=pod

=head2 PROGRAMS

These are the programs that are used by the NetDist scripts.

=over 4

=item $NDGEN

Contains the name of the script to generate Object files.

Default is ./Generate.

=back

=cut

$NDGEN="./Generate" if (!defined($NDGEN));
$ENV{'NDGEN'} = $NDGEN ;

=pod

=over 4

=item $NDAWK 

Contains the awk command.

Default is awk.

=back

=cut

$NDAWK="awk" if (!defined($NDAWK));
$ENV{'NDAWK'} = $NDAWK ;

=pod

=over 4

=item $NDRDIST

Contains the path to rdist.

Default is /usr/bin/rdist.

=back

=cut

$NDRDIST="$ENV{NDHOME}/bin/sdist" if ( !defined($NDRDIST));
$ENV{'NDRDIST'} = $NDRDIST ;

=pod

=over 4

=item $NDRSH

Contains the path to rsh.

Default is B<rsh>.  For HP-UX, you may want to use B<remsh>.  If you use SSH,
you can set this to B<ssh>.

=back

=cut

$NDRSH="$NDHOME/bin/ssh-nox" if ( !defined($NDRSH));
$ENV{'NDRSH'} = $NDRSH ;

=pod

=over 4

=item $NETG 

Contains a command that will expand a netgroup into hosts with no
domainname.

Default is C<"$NDHOME/bin/netg -hs -f $NDHOME/etc/netgroup">.

=back

=cut

$NETG="$NDHOME/bin/netg -hs -f $NDHOME/etc/netgroup";
$ENV{'NETG'} = $NETG ;

=pod

=over 4

=item $NETGU

Contains a command that will expand a netgroup into users.

Default is C<"$NDHOME/bin/netg -u -f $NDHOME/etc/netgroup">.

=back

=cut

$NETGU="$NDHOME/bin/netg -u -f $NDHOME/etc/netgroup";
$ENV{'NETGU'} = $NETGU ;
#
# END OF PROGRAMS
#
# START OF LOCAL ENVIRONMENT
#

=pod

=head2 LOCAL ENVIRONMENT

These variables will be different for each instance of a program.
This relies a great deal on the process id.  These variables are
B<NOT> exported to the %ENV array.

=over 4

=item $NDVERBOSE

Set to a number greater than 0 if running in verbose mode.

Default is 0.

=back

=cut

$NDVERBOSE=0 if (!defined($NDVERBOSE));

=pod

=over 4

=item $NDKEY 

The key used to lock files.  When lock files are created, this key is
written out to the lock file.  A short sleep is performed, then
the key is read back in.  If the key in the file matches $NDKEY,
then we have the lock and can proceed.

The key is set to the the string C<"$$/`uname -n`">, which should be
unique to each process being called.

=back

=cut

chop($NDKEY = "$$/" . `uname -n`);

=pod

=over 4

=item $NDMYNAME

Contains the name of script being run.  This is basically the
basename(1) of argv 0.  This name is used in some of the verbose
output routines to distinguish which process is printing the
errors.

=back

=cut

$NDMYNAME = $0;
$NDMYNAME =~ s-.*/([^/]*)$-$1-;

=over 4

=item @NDLOCKS

Contains a list of all locks.  At the end of a script, or when a signal
is caught, the CleanUp() function will remove all of these locks.

=item @NDTMPLOCKS

Contains a list of all temporary locks.  At the end of a script, or
when a signal is caught, the CleanUp() function will remove all of
these locks.

=item @NDTMPFILES

Contains a list of all temporary files.  At the end of a script, or
when a signal is caught, the CleanUp() function will remove all of
these files.

=back

=cut


#
# START GLOBAL FUNCTIONS
#

=pod

=head1 GLOBAL FUNCTIONS

These functions are available to all scripts and are used by each
other.

=cut
#
#
# Catch Signals.
#
# Stop on hangup, interrupt, quit, pipe-error, software termination.
# The CleanUp function requires that a list of open locks be kept in
# NDLOCKS and a list of temporary files be kept in NDTMPFILES.
sub SigHandler {
    # Simply call CleanUp with no args
    &CleanUp();
}
$SIG{'HUP'} = 'SigHandler';
$SIG{'INT'} = 'SigHandler';
$SIG{'QUIT'} = 'SigHandler';
$SIG{'PIPE'} = 'SigHandler';
$SIG{'TERM'} = 'SigHandler';


=pod

=over 4

=item errorexit()

Print an error message and exit with a value of 1.  Runs CleanUp() to
remove lock files and tmp files.

=back

=cut

sub errorexit  {
    &CleanUp($NDMYNAME);
    print "ERROR: ", join(" ",@_) . "\n";
    exit 1 ;
}

=pod

=over 4

=item DiffCopy($src_file,$dst_file) 

If the contents of I<src_file> are different than the contents of
I<dst_file>, then copy I<src_file> to I<dst_file>.  This procedure
uses the B<cmp>(1) command to compare the files.

This procedure should be used in Generate scripts to create files so
that timestamps on files are not changed if the file does not
change.

=back

    return 0 if any of the args passed is empty
    return 1 if files are the same
    return the value of 'system("cp $src $dst")' if a copy takes place

=cut

sub DiffCopy {
    my($src,$dst) = @_;

    return 0 if ( "$src" eq "" || "$dst" eq "" );
    return 0 if ( ! -f $src );
    if ( ! -f "$dst" ) {
	open(DST,">$dst") && close(DST);	# implement touch()
    }
    system "cmp -s $src $dst ";
    return 1 if ( $?/256 == 0 );
    unlink("$dst");
    system("cp $src $dst");
}


=pod

=over 4

=item DiffCopyStr($str,$dst_file)

If the contents of the string I<str> is different than the
contents in file I<dst_file>, then write out I<str> to I<dst_file>.

This procedure reads in the I<dst_file> as a single string and uses
the perl 'eq' op to do the comparison.

=back

    return 0 if any of the args passed is empty
    return 1 if the string and file are the same
    return 1 if a new files was written

=cut

sub DiffCopyStr {
    my($str,$dst) = @_;
    my($dststr,$oldfs);

    return 0 if ( "$str" eq "" || "$dst" eq "" );
    if ( -f "$dst" ) {
	if ( !open(DST,"$dst") ) {
	    warn "cannot open $dst for read:  $!\n";
	    return 0;
	}
	$oldfs = $/;
	undef $/;		# read in the entire file
	$dststr = <DST>;
	close DST;
	$/ = $oldfs;
    }
    return 1 if ( $dststr eq $str );	# they're the same
    #
    # Write out the new file
    if ( ! open(DST,">$dst") ) {
	warn "cannot open $dst for write:  $!\n";
	return 0;
    }
    print DST $str;
    close DST;
    return 1;
}

=pod

=over 4

=item CleanUp([$NDMYNAME])

Clean up locks listed in @NDLOCKS and @NDTMPLOCKS, and remove tmp
files in @NDTMPFILES.

This function is called in two instances:

=over 4

=item * From a signal handler

When called from the signal handler, no arguments are passed.
CleanUp() removes lock and tmp files and exits with a value of 1.

=item * At the end of a script

When called from the end of a script, the argument of $NDMYNAME will
be passed in.  CleanUp() removes lock and tmp files and returns to the
calling procedure.

=back

=back

=cut

sub CleanUp {
    #
    # If this is called via a signal, it'll have no arguments
    if ( $#_ < 0 ) {
	&VerbEchoC("Signal(caught:  cleaning up...");
    }
    else {
	&VerbEchoC(join(" ",@_) . ":  cleaning up...");
    }
    &VerbEchoC("removing locks...");
    &RmLockFile( $NDKEY , @NDLOCKS , @NDTMPLOCKS);
    &VerbEchoC("removing tmpfiles...");
    unlink ( @NDTMPFILES ) ;
    &VerbEcho("done");
    if ( $#_ < 0 ) {exit 1;}
}
#
# GetLockFile() -- this procedure actually gets and verifies a lock file.
#    This routine is called by the Get*Lock() routines
sub GetLockFile {
    my($key,$name) = @_;
    # Get some vars
    if ( "$key" eq "" ) {
	&ErrorEcho("GetLockFile() -- no key set");
	return 1;
    }
    if ( "$name" eq "" ) {
	&ErrorEcho("GetLockFile() -- no name set");
	return 1;
    }
    $key .= "\n";
    #
    # make sure the directory is there
    &errorexit("Cannot mkdir $NDLOCKDIR")
	if ( ! -d "$NDLOCKDIR" && !mkdir($NDLOCKDIR,0755));
    #
    # if the file is already there, exit 1
    return 1 if ( -f "$NDLOCKDIR/$name");
    #
    # create the file
    &errorexit("Cannot create $NDLOCKDIR/$name lock file")
	if ( ! open(OUTFILE,">$NDLOCKDIR/$name") );
    #
    # Write out our key
    print OUTFILE "$key" ;
    close OUTFILE;
    #
    # Now, to avoid timing conflicts, make sure our id is in that file
    &errorexit("Lost $NDLOCKDIR/$name file!!")
	if ( ! -f "$NDLOCKDIR/$name" || ! open(INFILE,"$NDLOCKDIR/$name"));
    $_ = <INFILE>;
    close(INFILE);
    return 1 if ( $_ ne $key );
    return 0;
}

=pod

=over 4

=item GetLock([$NAME])

Create a lock file.  If a lock is not available, wait a bit
and try again.  Otherwise, errorexit().  This is for important files,
like the $NDHOSTINST file.

If a $NAME argument is passed, this is the name of the lock.
If no lockname is given, then $NDMYNAME is used.

This function has the side effect of updating @NDLOCKS.

=back

=cut

#
sub GetLock {
    my ($ThisLock);
    #
    # If no name was passed, get a lock for this program.
    if ( $#_ < 0 ) { $ThisLock = $NDMYNAME; }
    else { $ThisLock = shift(@_); }
    push (@NDLOCKS,$ThisLock);
    &VerbEchoC("$NDMYNAME:  creating $ThisLock lock file . . . ");
    if ( &GetLockFile( $NDKEY, $ThisLock) != 0) {
	&VerbEchoC("exists -- waiting . . . ");
	sleep(10);
	if ( &GetLockFile($NDKEY,$ThisLock)!= 0) {
	    &errorexit("Cannot get lock file.  Another $NDMYNAME running?");
	}
    }
    &VerbEcho("done");
}

=pod

=over 4

=item RmLock([@locks])

Removes the locks specified.

=back

=cut

sub RmLock {
    &RmLockFile( $NDKEY , @_);
}

=pod

=over 4

=item GetTmpLock([$NAME])

Create a temporary lock file.  If the lock file is not available,
return to the calling procedure.

$NAME an optional name of the lock.  If no lockname is
given, then the name of the program is used.

This function has the side effect of setting @NDTMPLOCKS.

=back

=cut

#
sub GetTmpLock {
    my ($ThisLock);
    #
    # If no name was passed, get a lock for this program.
    if ( $#_ < 0 ) { $ThisLock = $NDMYNAME; }
    else { $ThisLock = shift(@_); }
    push(@NDTMPLOCKS,$ThisLock);
    return &GetLockFile($NDKEY,$ThisLock);
}
#
# RmLockFile() -- this procedure actually verifies and removes a file
#
sub RmLockFile {
    my($key,@files) = @_;
    # Get some vars

    if ( "$key" eq "" ) {
	&ErrorEcho("RmLockFile() -- no key set");
	return 1;
    }
    $key .= "\n";
    foreach $file ( @files ) {
	#
	# If the lock file's gone, then go to the next
	next if ( ! open(INFILE,"$NDLOCKDIR/$file") );
	#
	# Make sure it's our lockfile
	$_ = <INFILE>;
	close (INFILE);
	next if ( $_ ne $key );
	#
	# Ok, it's ours.  Remove it.
	unlink("$NDLOCKDIR/$file");
    }
    return 0;
}

=pod

=over 4

=item RmTmpLock()

Removes the temporary locks in @NDTMPLOCKS.

=back

=cut

sub RmTmpLock {
    &RmLockFile( $NDKEY , @NDTMPLOCKS);
}
#
# EchoC -- echo a string without the newline
sub EchoC {
    print join(" ",@_);
}
#
# VerbEcho() -- echo $* with newline if we're in verbose mode
sub VerbEcho {
    print STDERR join(" ",@_) . "\n" if ( $NDVERBOSE == 1 );
}
#
# VerbEchoC() -- echo $* with no newline if we're in verbose mode
sub VerbEchoC {
    print STDERR join(" ",@_)  if ( $NDVERBOSE == 1 );
}
#
# ErrorEcho() -- echo $* to stderr
sub ErrorEcho  {
    print STDERR join(" ",@_) . "\n";
}
#
# CheckDirs() -- check to make sure that the supplied directories
#    exist.  If not, exit with an error.
sub CheckDirs {
    my(@dirs) = @_;
    my($dir);

    foreach $dir (@dirs) {
	&errorexit("$NDMYNAME:  Cannot mkdir $dir")
	    if (! -d "$dir" && ! mkdir("$dir",0755));
    }
}
#
# inlist($key,@lst) -- return 1 if $key is in list @lst
sub inlist {
    my $key = shift;
    my %h = map { $_ => 1 } @_;
    return $h{$key};
}
#
# uniq -- take in a list and return a uniq list
sub uniq {
    my(@newlist,%seen);
    local $_;
    foreach ( @_ ) {
        next if ( /^$/ );               # no nulls
        next if ($seen{$_});		# no duplicates
        push(@newlist,$_);
	$seen{$_} = 1;
    }
    return @newlist;
}
#
# END GLOBAL FUNCTIONS
#
#
# Set the output to non-buffered, so that prompts and debug messages 
# get printed in a timely manner
$| = 1;
1;
