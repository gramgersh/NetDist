#
# $Id: Makefile 1343 2008-04-01 14:44:18Z erickson $
#
include $(NDHOME)/Makefile.common.subsystem

SUBDIRS=source

distclean:  lclean

lclean:
	find . -name '*~' -print | xargs /bin/rm -f
